package view;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() {

	}

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar datos de infracciones en movimiento");
		System.out.println("2. Mostrar todas las infracciones que terminaron en un accidente en cierta direcci�n (linear probing)");
		System.out.println("3. Mostrar todas las infracciones que terminaron en un accidente en cierta direcci�n (separate chaining)");
		System.out.println("4. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printDatosMuestra( int nMuestra, Comparable [ ] muestra)
	{
		for ( Comparable dato : muestra)
		{	System.out.println(  dato.toString() );    }
	}

	public void printMensage(String mensaje) {
		System.out.println(mensaje);
	}
}
