package controller;

import java.awt.List;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Collections;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;
import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import model.data_structures.*;
import model.util.Sort;
import model.vo.VOMovingViolation;
import view.MovingViolationsManagerView;

@SuppressWarnings("unused")
public class Controller {

	private MovingViolationsManagerView view;

	// DONE Definir las estructuras de datos para cargar las infracciones del periodo definido

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	//	private IQueue<VOMovingViolation> movingViolationsQueue;


	/**
	 * Tabla hash con modalidad linear probing
	 */
	private hashTableLinearProbing<Integer,IQueue<VOMovingViolation>> linearProbing;

	/**
	 * Tabla hash con modalidad separate chaining
	 */
	private hashTableSeparateChaining<Integer,IQueue<VOMovingViolation>> separateChainning;

	// Muestra obtenida de los datos cargados 
	private VOMovingViolation [ ] muestra;

	// Copia de la muestra de datos a ordenar 
	private VOMovingViolation[ ] muestraCopia;

	//Arreglo de los elementos cargados

	private VOMovingViolation[] arreglo = new VOMovingViolation[599207];

	//Clase de ordenamiento

	private Sort sort = new Sort();

	boolean carga = false;

	String objectId = "";

	String location = "";

	String ticketIssueDate ="";

	String totalPaid ="";

	String accidentIndicator ="";

	String violationDescription ="";

	String adressId ="";

	String fineAMT = "";

	String violationCode ="";


	public Controller() {
		view = new MovingViolationsManagerView();
		//DONE inicializar las estructuras de datos para la carga de informacion de archivos

		//movingViolationsQueue = new Queue<VOMovingViolation>();

		linearProbing =  new hashTableLinearProbing<>();

		separateChainning = new hashTableSeparateChaining<>();


	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe ser Comparable para ser usada en los ordenamientos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * @return numero de infracciones leidas 
	 */
	public int loadMovingViolations() {
		// TODO Los datos de los archivos deben guardarse en la Estructura de Datos definida

		int contador = 0;
		int cont = 0;
		carga = true;

		JsonReader reader;
		JsonParser parser = new JsonParser();

		try {

			String archJson = "./data/Moving_Violations_Issued_in_January_2018.json";


			System.out.println("Tamaño Inicial del arreglo linearProbing: \t"+linearProbing.darM());
			System.out.println("Tamaño Inicial del arreglo separateChainning: \t"+separateChainning.darM()+"\n");


			for(int i = 0; i < 6; i++) {

				JsonArray jsonArray = (JsonArray) parser.parse(new FileReader(archJson));

				for(int j = 0; j < jsonArray.size(); j++) {


					JsonObject jsonObject = (JsonObject) jsonArray.get(j);

					objectId = jsonObject.get("OBJECTID").getAsString();
					location = jsonObject.get("LOCATION").getAsString();
					ticketIssueDate = jsonObject.get("TICKETISSUEDATE").getAsString();
					fineAMT = jsonObject.get("FINEAMT").getAsString();
					totalPaid = jsonObject.get("TOTALPAID").getAsString();
					accidentIndicator = jsonObject.get("ACCIDENTINDICATOR").getAsString();
					violationDescription = jsonObject.get("VIOLATIONDESC").getAsString();
					violationCode = jsonObject.get("VIOLATIONCODE").getAsString();
					adressId = "0";

					try {
						adressId = jsonObject.get("ADDRESS_ID").getAsString();

					} catch (Exception e) {
						// TODO: handle exception
					}

					//Se crea el objeto VOMovingViolations

					VOMovingViolation infraccion = new VOMovingViolation(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription, adressId, fineAMT, violationCode);
					arreglo[j] = infraccion; 


					int keyAdressId = Integer.parseInt(adressId);



					if (linearProbing.contains(keyAdressId)) {

						IQueue<VOMovingViolation> lista = linearProbing.get(keyAdressId);
						lista.enqueue(infraccion);

					}
					else {

						IQueue<VOMovingViolation> lista  = new Queue<VOMovingViolation>();
						lista.enqueue(infraccion);
						linearProbing.put(keyAdressId, lista);


					}
					if (separateChainning.contains(keyAdressId)) {

						IQueue<VOMovingViolation> lista = separateChainning.get(keyAdressId);
						lista.enqueue(infraccion);

					}
					else {

						IQueue<VOMovingViolation> lista  = new Queue<VOMovingViolation>();
						lista.enqueue(infraccion);
						separateChainning.put(keyAdressId, lista);

					}

					cont++;

				}

				if(i==0)
				{
					archJson = "./data/Moving_Violations_Issued_in_February_2018.json";
				}else if(i ==1)
				{
					archJson ="./data/Moving_Violations_Issued_in_March_2018.json";
				} else if(i == 2) {

					archJson = "./data/Moving_Violations_Issued_in_April_2018.json";

				} else if(i == 3) {

					archJson ="./data/Moving_Violations_Issued_in_May_2018.json";

				} else if(i == 4) {

					archJson ="./data/Moving_Violations_Issued_in_June_2018.json";

				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}


		System.out.println("----------------------LinearProbing-------------------------");


		System.out.println("Tamaño final del arreglo: \t" + linearProbing.darM());
		System.out.println("Numero de tuplas K,V: \t" + linearProbing.size());
		System.out.println("No de rehashes: \t" + linearProbing.darContRehashes());
		System.out.println("Factor de carga final: \t" + (double)linearProbing.size()/(double)linearProbing.darM());


		System.out.println("----------------------SeparateChainning---------------------");

		System.out.println("Tamaño final del arreglo: \t" + separateChainning.darM());
		System.out.println("Numero de tuplas K,V: \t" + separateChainning.size());
		System.out.println("No de rehashes: \t" + separateChainning.darContRehashes());
		System.out.println("Factor de carga final: \t" + separateChainning.size()/separateChainning.darM());
		System.out.println();

		return cont;
	}


	/**
	 * Se muestran	todas	las	infracciones	que	terminaron	en	un	accidente en	esa	dirección.	El	
	 * resultado	debe	estar	ordenado	cronológicamente. Se utiliza una tabla hash linear probing.
	 * @param pAdress, dirección de las infracciones
	 */

	public void infraccionesAccidenteLP(Integer pAdress) {

		IQueue<VOMovingViolation> colaResultante = linearProbing.get(pAdress);
		VOMovingViolation[] array = new VOMovingViolation[colaResultante.size()];
		VOMovingViolation[] arrayAux = new VOMovingViolation[colaResultante.size()];

		int i = 0;

		for(VOMovingViolation actual: colaResultante) {

			array[i++] = actual;

		}


		sort.ordenarMergeSort(array, arrayAux, 0, array.length-1);

		for(VOMovingViolation actual: array) {

			if(actual.getAccidentIndicator().equals("Yes")) {
				System.out.println(actual.objectId() +  "\t" + actual.getLocation() + "\t" + actual.getTicketIssueDate() + "\t" + actual.getViolationCode() + "\t" + actual.getFineAMT());
			}


		}


	}


	/**
	 * Se muestran	todas	las	infracciones	que	terminaron	en	un	accidente en	esa	dirección.	El	
	 * resultado	debe	estar	ordenado	cronológicamente. Se utiliza una tabla hash separate chaining.
	 * @param pAdress, dirección de las infracciones
	 */

	public void infraccionesAccidenteSC(Integer pAdress) {

		IQueue<VOMovingViolation> colaResultante = separateChainning.get(pAdress);
		VOMovingViolation[] array = new VOMovingViolation[colaResultante.size()];
		VOMovingViolation[] arrayAux = new VOMovingViolation[colaResultante.size()];

		int i = 0;

		for(VOMovingViolation actual: colaResultante) {

			array[i++] = actual;

		}


		sort.ordenarMergeSort(array, arrayAux, 0, array.length-1);

		for(VOMovingViolation actual: array) {

			if(actual.getAccidentIndicator().equals("Yes")) {
				System.out.println(actual.objectId() +  "\t" + actual.getLocation() + "\t" + actual.getTicketIssueDate() + "\t" + actual.getViolationCode() + "\t" + actual.getFineAMT());
			}


		}


	}



	/**
	 * M�todo run
	 */
	public void run() {
		long startTime;
		long endTime;
		long duration;

		int nDatos = 0;
		int nMuestra = 0;
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				// Cargar infracciones

				if(carga == true) {

					view.printMensage("Los datos ya se habian cargado.");

				} else {
					nDatos = this.loadMovingViolations();
					view.printMensage("Numero infracciones cargadas:" + nDatos);
				}
				break;

			case 2:

				if(carga != true) {

					view.printMensage("Debe cargar primero las infracciones");

				} else {

					view.printMensage("Digite la dirección que desea consultar");
					int resultado = sc.nextInt();
					infraccionesAccidenteLP(resultado);

				}

				break;

			case 3:

				if(carga != true) {

					view.printMensage("Debe cargar primero las infracciones");

				} else {

					view.printMensage("Digite la dirección que desea consultar");
					int resultado = sc.nextInt();
					infraccionesAccidenteSC(resultado);

				}

				break;

			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

}
