package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	// DONE Definir los atributos de una infraccion
	private int objectId;

	private String location;

	private String ticketIssueDate;

	private double totalPaid;

	private String accidentIndicator;

	private String violationDescription;

	private int adressId;

	private double fineAMT;

	private String violationCode;


	/**
	 * Metodo constructor
	 */
	public VOMovingViolation( String pObjectId, String pLocation, String pTicketIssueDate, String pTotalPaid, String pAccidentIndicator, String pViolationDescription, String pAdressId, String pFineAMT, String pViolationCode)
	{
		// DONE Implementar
		objectId = Integer.parseInt(pObjectId);
		location = pLocation;
		ticketIssueDate = pTicketIssueDate;
		totalPaid = Double.parseDouble(pTotalPaid);
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;
		adressId = Integer.parseInt(pAdressId);
		fineAMT = Double.parseDouble(pFineAMT);
		violationCode = pViolationCode;

	}	

	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public int objectId() {
		return objectId;
	}	


	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente paga el que recibio la infraccion en USD.
	 */
	public double getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return fineAMT
	 */

	public double getFineAMT() {

		return fineAMT;

	}

	/**
	 * retorna el c�digo de la infracci�n
	 */

	public String getViolationCode() {

		return violationCode;

	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() {
		return violationDescription;
	}

	/**
	 * @return adress id de la infracci�n.
	 */
	public int getAdressId() {

		return adressId;

	}

	@Override
	public int compareTo(VOMovingViolation o) {
		return o.getTicketIssueDate().compareTo(ticketIssueDate);
	}


	//	public int compareToRegisters(VOMovingViolation o) {
	//		int deter = 90;
	//		if(o.getAdressId() == adressId)
	//		{
	//			deter = 0;
	//
	//		}else if(o.getAdressId() < adressId)
	//		{
	//			deter = 1;
	//		}else if(o.getAdressId() > adressId)
	//		{
	//			deter = -1;
	//		}
	//		return deter;
	//	}

	public String toString()
	{
		return objectId+"-"+"-"+getLocation()+"-"+getTicketIssueDate()+"-"+getTotalPaid()+"-"+getAccidentIndicator()+"-"+getViolationDescription();
	}
}
