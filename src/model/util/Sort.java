package model.util;

import model.vo.VOMovingViolation;

/**
 * Metodos impelemtados de Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne
 */
public class Sort {


	//----------------------------------------------------------------------------------------------------------

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	private static void merge( VOMovingViolation[ ] datos, VOMovingViolation[ ] aux, int lo,int mid ,int hi ) 
	{
		assert isSorted(datos, lo, mid);
		assert isSorted(datos, mid+1, hi);

		// copy to aux[]
		for (int k = lo; k <= hi; k++) 
		{
			aux[k] = datos[k]; 
		}

		// merge back to a[]
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if      (i > mid)              datos[k] = aux[j++];
			else if (j > hi)               datos[k] = aux[i++];
			else if (less(aux[j], aux[i])) datos[k] = aux[j++];
			else                           datos[k] = aux[i++];
		}

		// postcondition: a[lo .. hi] is sorted
		assert isSorted(datos, lo, hi);
	}


	public static void ordenarMergeSort(VOMovingViolation[] a, VOMovingViolation[] aux, int lo, int hi) 
	{
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		ordenarMergeSort(a, aux, lo, mid);
		ordenarMergeSort(a, aux, mid + 1, hi);
		merge(a, aux, lo, mid, hi);
	}
	private static void odenarMergeSort(VOMovingViolation[] a) 
	{
		VOMovingViolation[] aux = new VOMovingViolation[a.length];
		ordenarMergeSort(a, aux, 0, a.length-1);
		assert isSorted(a);
	}

	private static boolean isSorted(VOMovingViolation[] a) {
		return isSorted(a, 0, a.length - 1);
	}

	private static boolean isSorted(VOMovingViolation[] a, int lo, int hi) {
		for (int i = lo + 1; i <= hi; i++)
			if (less(a[i], a[i-1])) return false;
		return true;
	}




	//----------------------------------------------------------------------------------------------------------

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(VOMovingViolation v, VOMovingViolation w)
	{
		// DONE implementar
		return (v.compareTo(w) > 0);
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( VOMovingViolation[] datos, int i, int j)
	{
		// DONE implementar
		VOMovingViolation swap = datos[i];
		datos[i] = datos[j];
		datos[j] = swap;
	}

}
