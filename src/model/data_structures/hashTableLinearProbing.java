/**
 * Este c�digo se bas� en: 
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *  For other implementations, see {@link ST}, {@link BinarySearchST},
 *  {@link SequentialSearchST}, {@link BST}, {@link RedBlackBST}, and
 *  {@link LinearProbingHashST},
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */


package model.data_structures;

public class hashTableLinearProbing<Key,Value> 
{
	private static int CAPACIDAD_INICIAL = 97;

	private int n;           // numero de key-value pares en la tabla
	private int m;           // tama�o de la tabla hash
	private Key[] keys;      // las llaves
	private Value[] vals;    // los valores

	private int contRehashes;

	/**
	 * Inicializa una tabla vac�a
	 */
	public hashTableLinearProbing() {
		this(CAPACIDAD_INICIAL);
	}

	/**
	 * Inicializa una tabla vac�a con un tama�o inicial espec�fico.
	 *
	 * @param capacidad inicial
	 */
	public hashTableLinearProbing(int capacity) {
		CAPACIDAD_INICIAL =capacity;
		m = capacity;
		n = 0;
		keys = (Key[])   new Object[m];
		vals = (Value[]) new Object[m];
		contRehashes = 0;
	}

	/**
	 * Retorna el numero de llave en la tabla .
	 * @return numero de key-value pares en la tabla
	 */
	public int size() {
		return n;
	}

	/**
	 * Retorna true si la tabla est� vac�a.
	 *
	 * @return  true true si la tabla est� vac�a o false de lo contrario
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Retorna true si la tabla tiene la llave espec�fica .
	 *
	 * @param  key la llave
	 * @return true si la conteiene, false de lo contrario
	 * @throws IllegalArgumentException 
	 */
	public boolean contains(Key key) {
		if (key == null) throw new IllegalArgumentException("el argumento es nulo");
		return get(key) != null;
	}


	// funcion hash para keys - returns value between 0 and M-1
	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % m;
	}

	// resizes la tabla hash a la capacidad dada re-hashing todas las keys
	private void resize(int capacity) {
		contRehashes++;
		hashTableLinearProbing<Key, Value> temp = new hashTableLinearProbing <Key, Value>(capacity);
		for (int i = 0; i < m; i++) {
			if (keys[i] != null) {
				temp.put(keys[i], vals[i]);
			}
		}
		keys = temp.keys;
		vals = temp.vals;
		m    = temp.m;
	}

	/**
	 * Dar tama�o de la tabla hash linear probing
	 * @return tama�o de la tabla
	 */

	public int darM() {

		return m;

	}

	/**
	 * Inserta una key-value par en la tabla hash, sobre escribiendo el valor viejo con el nuevo valor si la tabla hash ya contiene la llave espec�fica b
	 * borra la llave especifica (y su valor asociado) de la tabla hash si el valor especifico es null
	 * @param  key la llave
	 * @param  val el valor
	 * @throws IllegalArgumentException si key es null
	 */
	public void put(Key key, Value val) {
		if (key == null) throw new IllegalArgumentException("el primer argumento a poner es null");

		if (val == null) {
			delete(key);
			return;
		}

		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % m) {
			if (keys[i].equals(key)) {
				vals[i] = val;
				return;
			}
		}
		keys[i] = key;
		vals[i] = val;
		n++;

		// Dobla la tabla si el tama�o es 75% lleno
		if (n >= 3*m/4) 
		{
			contRehashes++;
			resize(2*m);
		}
	}

	/**
	 * Retorna el valor asociado con su llave espec�fica.
	 * @param key la llave
	 * @return el valor asociado a la llave o null si no hay valor;
	 * @throws IllegalArgumentExceptions si key es null
	 */
	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("el argumento es nulo");
		for (int i = hash(key); keys[i] != null; i = (i + 1) % m)
			if (keys[i].equals(key))
				return vals[i];
		return null;
	}

	/**
	 * Remueve una llave espec�fica con su valor asociado    
	 * (Si la llave est� en la tabla de simbolos).    
	 *
	 * @param  key la llave
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void delete(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");
		if (!contains(key)) return;

		// Encuentra la posicion i de la llave
		int i = hash(key);
		while (!key.equals(keys[i])) {
			i = (i + 1) % m;
		}
		// borra key y su valor asociado
		keys[i] = null;
		vals[i] = null;

		// rehash todas las llaves en el mismo campo
		i = (i + 1) % m;
		while (keys[i] != null) {
			// borra keys[i] y vals[i] y los re-inserta
			contRehashes++;
			Key   keyToRehash = keys[i];
			Value valToRehash = vals[i];
			keys[i] = null;
			vals[i] = null;
			n--;
			put(keyToRehash, valToRehash);
			i = (i + 1) % m;
		}

		n--;

		// halves size of array if it's 12.5% full or less
		if (n > 0 && n <= m/8) resize(m/2);

		assert check();
	}

	/**
	 * Retorna la cantidad de veces que se hizo rehash
	 * @returnc cantidad de veces que se hizo rehash
	 */
	public int darContRehashes() {

		return contRehashes;

	}


	/**
	 * Returns all keys in this symbol table as an {@code Iterable}.
	 * To iterate over all of the keys in the symbol table named {@code st},
	 * use the foreach notation: {@code for (Key key : st.keys())}.
	 *
	 * @return all keys in this symbol table
	 */
	public Iterable<Key> keys() {
		Queue<Key> queue = new Queue<Key>();
		for (int i = 0; i < m; i++)
			if (keys[i] != null) queue.enqueue(keys[i]);
		return queue;
	}

	// integrity check - don't check after each put() because
	// integrity not maintained during a delete()
	private boolean check() {

		// check that hash table is at most 75% full
		if (n >= 3*m/4) {
			System.err.println("Hash table size m = " + m + "; array size n = " + n);
			return false;
		}

		// check that each key in table can be found by get()
		for (int i = 0; i < m; i++) {
			if (keys[i] == null) continue;
			else if (get(keys[i]) != vals[i]) {
				System.err.println("get[" + keys[i] + "] = " + get(keys[i]) + "; vals[i] = " + vals[i]);
				return false;
			}
		}
		return true;
	}
}
