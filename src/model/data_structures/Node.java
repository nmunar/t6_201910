package model.data_structures;

public class Node<Key> {


	private Key elemento;

	private Node<Key> siguiente;

	private Node<Key> anterior;

	public Node(Key pElemento) {

		elemento = pElemento;
		anterior = null;
		siguiente = null;

	}


	public Node<Key> darSiguiente() {

		return siguiente;

	}

	public Node<Key> darAnterior()
	{

		return anterior;

	}


	public void cambiarSiguiente(Node<Key> pSiguiente) {

		siguiente = pSiguiente;

	}

	public void cambiarAnterior(Node<Key> pAnterior) {

		anterior = pAnterior;

	}

	public Key darElemento() {

		return elemento;

	}


}