package model.data_structures;
import java.util.Iterator;
public class Queue<Key> implements IQueue<Key>
{
	private Node<Key> primero;
	private Node<Key> ultimo;
	private Node<Key> actual;
	private int longitud;

	public Queue() 
	{ 
		int longitud = 0 ;
		primero = null ;
		ultimo = null;
		actual=primero;
	} 

	@Override
	public Iterator iterator() {
		return new IteratorSQ<Key>(primero);
	}

	@Override
	public boolean isEmpty() 
	{ 
		return primero==null;
	}

	@Override
	public int size() 
	{
		return longitud;
	}

	@Override
	public void enqueue(Key t) 
	{  
		Node<Key> agregado = new Node<Key>((Key) t);
		if(primero==null)
		{  primero =  agregado;
		ultimo= agregado;
		longitud++;
		}else{
			agregado.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(agregado);
			ultimo = agregado;
			longitud++;
		} 
	}

	public Node<Key> darUltimo()
	{
		return ultimo;
	}

	public Node<Key> darPrimero()
	{
		return primero;
	}
	public Node<Key> darActual()
	{
		return actual;
	}

	@Override
	public Key dequeue() 
	{  
		Node<Key> actual = primero;
		Node<Key> siguiente = actual.darSiguiente();
		if(primero==null)
		{  }else  { 
			siguiente.cambiarAnterior(null);
			primero = siguiente;
			return actual.darElemento();
		}
		return actual.darElemento();

	}

}
