package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorSQ<Key> implements Iterator<Key> 
{

	private Node<Key> siguiente;



	public IteratorSQ(Node<Key> primero)
	{

		siguiente = primero;
	}
	/**
	 * 
	 */
	@Override
	public boolean hasNext() 
	{
		return siguiente != null;
	}


	@Override
	public Key next() 
	{
		if(siguiente == null)
			throw new NoSuchElementException("No hay proximo");

		Key elemento = siguiente.darElemento();
		siguiente = siguiente.darSiguiente();
		return elemento;	
	}

	public Key previous() 
	{
		if(siguiente.darAnterior() == null)
			throw new NoSuchElementException("No hay anterior");

		Key elemento = siguiente.darAnterior().darElemento();

		return elemento;	
	}

	/**
	 * Eliminar el ultimo nodo visitado(es decir, el nodo anterior al nodo proximo)
	 */
	public void remove() throws UnsupportedOperationException, IllegalStateException
	{

		throw new UnsupportedOperationException("No implementada");
	}

}
