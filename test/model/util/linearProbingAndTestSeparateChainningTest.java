package model.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;
import org.junit.Test;

import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.hashTableLinearProbing;
import model.data_structures.hashTableSeparateChaining;
import model.vo.VOMovingViolation;

public class linearProbingAndTestSeparateChainningTest 
{
	private VOMovingViolation[] datos;

	private VOMovingViolation[] datos1;

	private hashTableLinearProbing<Integer,VOMovingViolation> linear = new hashTableLinearProbing<Integer,VOMovingViolation>();
	private hashTableLinearProbing<Integer,IQueue<VOMovingViolation>> linear2 = new hashTableLinearProbing<Integer,IQueue<VOMovingViolation>>();
	private hashTableSeparateChaining<Integer,VOMovingViolation> separate = new hashTableSeparateChaining<Integer,VOMovingViolation>();
	private hashTableSeparateChaining<Integer,IQueue<VOMovingViolation>> separate2 = new hashTableSeparateChaining<Integer,IQueue<VOMovingViolation>>();

	private IQueue<VOMovingViolation> extreme;


	public void escenarioExtremeLin()
	{
		for (int i = 0; i < 10000; i++) 
		{
			VOMovingViolation dats = new VOMovingViolation(Integer.toString(i), "e", "2018-03-24T18:55:00.000Z","0","T120","v", Integer.toString(i),"1" , "A");

			int keyAdressId = dats.getAdressId();

			if (linear2.contains(keyAdressId)) {


				IQueue<VOMovingViolation> lista = linear2.get(keyAdressId);
				lista.enqueue(dats);

			}
			else {

				IQueue<VOMovingViolation> lista  = new Queue<VOMovingViolation>();
				lista.enqueue(dats);
				linear2.put(keyAdressId, lista);


			}
		}
	}
	public void escenarioExtremeSep()
	{
		for (int i = 0; i < 10000; i++) 
		{
			VOMovingViolation dats = new VOMovingViolation(Integer.toString(i), "e", "2018-03-24T18:55:00.000Z","0","T120","v", Integer.toString(i),"1" , "A");

			int keyAdressId = dats.getAdressId();

			if (separate2.contains(keyAdressId)) {


				IQueue<VOMovingViolation> lista = separate2.get(keyAdressId);
				lista.enqueue(dats);

			}
			else {

				IQueue<VOMovingViolation> lista  = new Queue<VOMovingViolation>();
				lista.enqueue(dats);
				separate2.put(keyAdressId, lista);


			}
		}
	}

	@Test
	public void testPutExtremeLinearProbing() 
	{
		long inicio = System.currentTimeMillis();
		escenarioExtremeLin();
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de put() LinaerProbing es : \n");
		System.out.println(finTiempo-inicio);
		assertTrue(linear2.size()==10000);
	}

	@Test
	public void testPutExtremeSeparateChainning() 
	{
		long inicio = System.currentTimeMillis();
		escenarioExtremeSep();
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de put() SeparateChainnig es : \n");
		System.out.println(finTiempo-inicio);
		assertTrue(separate2.size()==10000);
	}

	@Test
	public void getExtremeLinearProbing()
	{
		escenarioExtremeLin();
		long inicio = System.currentTimeMillis();
		for (int i = 0; i < linear2.size(); i++) 
		{
			linear2.get(i);
		}
		long finTiempo = System.currentTimeMillis();

		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de get () LinaerProbing es : \n");
		System.out.println(finTiempo-inicio);
	}


	@Test
	public void getExtremeSeparateChainning()
	{
		escenarioExtremeSep();
		long inicio = System.currentTimeMillis();
		for (int i = 0; i < separate2.size(); i++) 
		{
			separate2.get(i);
		}
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de get() SeparateChainning es : \n");
		System.out.println(finTiempo-inicio);


	}


	@Test
	public void testDeleteLinearProbing() {

		VOMovingViolation dats = new VOMovingViolation("1", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A");
		linear.put(22, dats);

		assertEquals(1, linear.size());

		linear.delete(22);

		assertEquals(0, linear.size());


	}

	@Test
	public void testDeleteSeparateChaining() {

		VOMovingViolation dats = new VOMovingViolation("1", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A");
		separate.put(22, dats);

		assertEquals(1, separate.size());

		separate.delete(22);

		assertEquals(0, separate.size());


	}

}
