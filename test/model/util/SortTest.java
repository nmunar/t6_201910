package model.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.vo.VOMovingViolation;

public class SortTest 
{

	// Muestra de datos a ordenar
	private VOMovingViolation[] datos;

	public void escenario1()
	{
		datos= new VOMovingViolation[6];
		VOMovingViolation dat1 = new VOMovingViolation("14744879", "600 BLK NEW YORK AVENUE NE W/B", "2018-03-24T18:55:00.000Z", "0", "T120","SPEED 16-20 MPH OVER THE SPEED LIMIT", "3", "1", "A");
		datos[0]=dat1;
		VOMovingViolation dat2 = new VOMovingViolation("14622622", "600 BLK MISSOURI AVE NW SE/B", "2018-03-08T14:35:00.000Z", "0", "T119","SPEED 11-15 MPH OVER THE SPEED LIMIT", "31","1", "A");
		datos[1]=dat2;
		VOMovingViolation dat3 = new VOMovingViolation("14622326", "KANSAS AVE NE/B @ BUCHANAN ST NW", "2018-03-03T15:25:00.000Z", "0", "T128","PASSING STOP SIGN WITHOUT COMING TO A FULL STOP", "1", "1","A");
		datos[2]= dat3;
		VOMovingViolation dat4 = new VOMovingViolation("14401305", "1400 BLK SOUTHERN AVE SE SW/B", "2018-02-08T12:55:00.000Z", "0", "T120","PASSING STOP SIGN WITHOUT COMING TO A FULL STOP", "100","1", "A");
		datos[3]=dat4;
		VOMovingViolation dat5 = new VOMovingViolation("13237919", "600 BLK KENILWORTH AVE NE S/B", "2018-01-10T21:02:00.000Z", "0", "T110","SPEED 11-15 MPH OVER THE SPEED LIMIT","2", "1","A");
		datos[4]=dat5;
		VOMovingViolation dat6 = new VOMovingViolation("14466623", "1400 BLK S CAPITOL ST SW S/B", "2018-01-27T21:08:00.000Z", "100", "T119","SPEED 11-15 MPH OVER THE SPEED LIMIT","1000", "1","A");
		datos[5]=dat6;

	}

	@Test
	public void testMerge() {
		escenario1();
		VOMovingViolation[]aux=new VOMovingViolation[datos.length];
		Sort.ordenarMergeSort(datos, aux, 0, datos.length-1);
		for (int i = 0; i < aux.length-1; i++) {
			if(datos[i].compareTo(datos[i+1])<1)
			{
				fail("No qued� ordenado el arreglo");
			}
		}

	}
}